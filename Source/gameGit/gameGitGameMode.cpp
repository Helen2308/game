// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "gameGitGameMode.h"
#include "gameGitHUD.h"
#include "gameGitCharacter.h"
#include "UObject/ConstructorHelpers.h"

AgameGitGameMode::AgameGitGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AgameGitHUD::StaticClass();
}
